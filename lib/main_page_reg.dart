import 'package:flutter/material.dart';
import 'profile.dart';

void main() {
  runApp(ContactProfilePage());
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.white10,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),
    actions: <Widget>[
      IconButton(
          onPressed: () {}, icon: Icon(Icons.star_border), color: Colors.black)
    ],
  );
}

//----------------------------------------------------------------
// class ContactProfilePage extends StatelessWidget {
//   const ContactProfilePage({Key? key}) : super(key: key);
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       children: <Widget>[
//         Column(
//           children: <Widget>[
//             Container(
//               width: double.infinity,
//               //Height constraint at Container widget level
//               height: 250,
//               child: Image.network(
//                 "https://images2.minutemediacdn.com/image/upload/c_crop,w_1920,h_1080,x_0,y_0/c_fill,w_1440,ar_16:9,f_auto,q_auto,g_auto/images/voltaxMediaLibrary/mmsport/mentalfloss/01gmb6jgpgvxfxb8n4px.jpg",
//                 fit: BoxFit.cover,
//               ),
//             ),
//             Container(
//               height: 60,
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 children: <Widget>[
//                   Padding(
//                     padding: EdgeInsets.all(8.0),
//                     child: Text(
//                       "Buggi Otis",
//                       style: TextStyle(fontSize: 28),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//             Divider(
//               color: Colors.grey,
//             ),
//             Container(
//               margin: EdgeInsets.only(top: 8, bottom: 8),
//               child: Theme(
//                 data: ThemeData(
//                   iconTheme: IconThemeData(color: Colors.cyan),
//                 ),
//                 child: profileActionItems(),
//               ),
//             ),
//             Divider(
//               color: Colors.grey,
//             ),
//             mobilePhoneListTile(),
//             otherPhoneListTile(),
//             emailListTile(),
//             directListTile(),
//             Divider(
//               color: Colors.grey,
//             ),
//           ],
//         ),
//       ],
//     );
//   }
// }

Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("330-803-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.red,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(" "),
    title: Text("440-885-3241"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.red,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("buggi@otis.com"),
    subtitle: Text("work"),
  );
}

Widget directListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("23456 Sunset St.Burlingame"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.redAccent,
      onPressed: () {},
    ),
  );
}

//----------------------------------

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildProfileButton(),
      buildCalendarButton(),
      buildTableButton(),
      buildMessageButton(),
      buildRecentButton(),
      buildTableViewButton()
    ],
  );
}

class ContactProfilePage extends StatelessWidget {
  const ContactProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  //Height constraint at Container widget level
                  height: 180,
                  child: Image.network(
                    "https://upload.wikimedia.org/wikipedia/th/thumb/2/27/Buu0001.jpeg/420px-Buu0001.jpeg",
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  height: 60,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "ประกาศสำคัญ!!",
                          style:
                              TextStyle(color: Colors.pinkAccent, fontSize: 30),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 40,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "กำหนดการชำระค่าบำรุงหอพักภาคฤดูร้อน "
                          "ปีการศึกษา 2565",
                          style: TextStyle(fontSize: 15),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                    width: 450,
                    //Height constraint at Container widget level
                    height: 250,
                    child: Image.network(
                        "https://i0.wp.com/www.sakaeo.buu.ac.th/2017/wp-content/uploads/2023/01/%E0%B8%87%E0%B8%B2%E0%B8%99%E0%B8%81%E0%B8%B4%E0%B8%88%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%99%E0%B8%B4%E0%B8%AA%E0%B8%B4%E0%B8%95-16012566-03.png?resize=450%2C250&ssl=1",
                        errorBuilder: (context, url, error) =>
                            Icon(Icons.error))),
                Divider(
                  color: Colors.grey,
                ),
                Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildProfileButton(),
                      buildCalendarButton(),
                      buildTableButton(),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildMessageButton(),
                      buildTableViewButton(),
                      buildRecentButton(),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Widget buildProfileButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.account_box_rounded,
          color: Colors.pink.shade300,
        ),
        onPressed: () {
          // Navigator.push(
          // context,
          //     MaterialPageRoute(builder: (context) => StudentProfile()),
          //    );
        },
      ),
      Text("บัญชีผู้ใช้งาน"),
    ],
  );
}

Widget buildCalendarButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.edit_calendar_rounded,
          color: Colors.pink.shade300,
        ),
        onPressed: () {},
      ),
      Text("ตรวจสอบวันจบ"
          "การศึกษา"),
    ],
  );
}

Widget buildTableButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.list_alt_outlined,
          color: Colors.pink.shade300,
        ),
        onPressed: () {},
      ),
      Text("ตารางเรียน/สอบ"),
    ],
  );
}

Widget buildMessageButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          color: Colors.pink.shade300,
        ),
        onPressed: () {},
      ),
      Text("ภาระค่าใช้จ่ายทุน"),
    ],
  );
}

Widget buildRecentButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.recent_actors,
          color: Colors.pink.shade300,
        ),
        onPressed: () {},
      ),
      Text("หลักสูตรที่เปิดสอน"),
    ],
  );
}

Widget buildTableViewButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.table_view,
          color: Colors.pink.shade300,
        ),
        onPressed: () {},
      ),
      Text("ปฎิทินการศึกษา"),
    ],
  );
}

//--------------------------------------------------
enum APP_THEME { LIGHT, DARK }

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white10,
        iconTheme: IconThemeData(
          color: Colors.deepPurple[50],
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade500,
      ),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.black54,
        iconTheme: IconThemeData(
          color: Colors.deepPurple[50],
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade500,
      ),
    );
  }
}

// class ContactProfilePage extends StatefulWidget {
//   @override
//   State<ContactProfilePage> createState() => _ContactProfilePageState();
// }
//
// class _ContactProfilePageState extends State<ContactProfilePage> {
//   var currentTheme = APP_THEME.LIGHT;
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       theme: currentTheme == APP_THEME.DARK
//           ? MyAppTheme.appThemeLight()
//           : MyAppTheme.appThemeDark(),
//       home: Scaffold(
//         appBar: buildAppBarWidget(),
//         body: buildBodyBarWidget(),
//         floatingActionButton: FloatingActionButton(
//           child: Icon(Icons.app_shortcut_sharp),
//           onPressed: () {
//             setState(() {
//               currentTheme == APP_THEME.DARK
//                   ? currentTheme = APP_THEME.LIGHT
//                   : currentTheme = APP_THEME.DARK;
//             });
//           },
//         ),
//       ),
//     );
//   }
// }
