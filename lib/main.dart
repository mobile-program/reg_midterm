import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'main_page_reg.dart';
import 'profile.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const RegApp());
}

class RegApp extends StatelessWidget {
  const RegApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        theme: ThemeData(
          primarySwatch: Colors.pink,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('REG BUU'),
          ),
          // drawer: Container(
          //   width: 250,
          //   color: Colors.red.shade200,
          //   // margin: EdgeInsets.only(top: 15, bottom: 8),
          //   child: Row(
          //    // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //    children: <Widget>[
          //       buildTableViewButton(),
          //
          //     ],
          //    ),
          // ),
          drawer: Drawer(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: Text('Thanyarak Namwong'),
                  accountEmail: Text('62160272@go.buu.ac.th'),
                  currentAccountPicture: CircleAvatar(
                    child: Text("T"),
                    foregroundColor: Colors.black,
                    backgroundColor: Colors.purpleAccent,
                  ),
                  otherAccountsPictures: <Widget>[],
                ),
                ListTile(
                  title: Text('ประวัตินิสิต'),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => StudentProfile()),
                    );
                  },
                ),
                ListTile(
                  title: Text('ตรวจสอบวันจบการศึกษา'),
                  onTap: () {},
                ),
                ListTile(
                  title: Text('ปฎิทินการศึกษา'),
                  onTap: () {},
                ),
                ListTile(
                  title: Text('ออกจากระบบ'),
                  onTap: () {},
                ),
                ListTile(
                  leading: Icon(Icons.call),
                  title: Text("021-566-4584"),
                  subtitle: Text("ติดต่อสอบถาม"),
                  trailing: IconButton(
                    icon: Icon(Icons.message),
                    color: Colors.red,
                    onPressed: () {},
                  ),
                ),
                Divider(),
                // Expanded(
                //   child: Align(
                //     alignment: Alignment.bottomLeft,
                //     child: ListTile(
                //       title: Text('ออกจากระบบ'),
                //       onTap: () {},
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
          body: const ContactProfilePage(),
        ),
      ),
    );
  }
}

Widget buildTableViewButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.table_view,
          color: Colors.pink.shade300,
        ),
        onPressed: () {},
      ),
      Text("ปฎิทินการศึกษา"),
    ],
  );
}

//------------------------------------------------------------
class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white10,
        iconTheme: IconThemeData(
          color: Colors.deepPurple[50],
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade500,
      ),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.black54,
        iconTheme: IconThemeData(
          color: Colors.deepPurple[50],
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.indigo.shade500,
      ),
    );
  }
}
